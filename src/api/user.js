import request from '@/utils/request'

//密码登录
export function login(data) {
  return request({
    url: '/auth/system-login',
    method: 'post',
    data
  })
}

export function getInfo(data) {
  return request({
    url: '/admin/profile/info',
    method: 'post',
    data
  })
}

export function logout() {
  return request({
    url: '/auth/logout',
    method: 'post'
  })
}

//发送验证码
export function smsPhone(data) {
  return request({
    url: '/api/biz/sms/phone',
    method: 'post',
		data
  })
}

//短信登录
export function smsLogin(data) {
  return request({
    url: '/auth/system-sms-login',
    method: 'post',
    data
  })
}