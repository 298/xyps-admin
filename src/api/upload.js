import request from '@/utils/request'

export default {
  url: process.env.VUE_APP_BASE_API + '/admin/component/oss/upload',
  base64Url: process.env.VUE_APP_BASE_API + '/admin/component/oss/base64Upload'
}

export function uploadBase(data) {
  return request({
    url: '/admin/component/oss/base64Upload',
    method: 'post',
    data
  })
}
