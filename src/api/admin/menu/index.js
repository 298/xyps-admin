import request from '@/utils/request'

export default {
	//角色菜单列表
	resourceMenuTree(data) {
		return request({
			url: '/admin/system/resource/menuTree',
			method: 'post',
			data
		})
	}
} 