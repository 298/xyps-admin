import request from '@/utils/request'

export function page(data) {
	return request({
		url: '/admin/member/dormitory/page',
    method: 'post',
    data
  })
}

//查询所有学校信息
export function schoolList(data) {
  return request({
    url: '/admin/system/area/areaAllList',
    method: 'get',
    params : data
  })
}

//查询所有用户信息
export function memberList(data) {
  return request({
    url: '/admin/member/info/list',
    method: 'get',
    params : data
  })
}

export function detail(data) {
  return request({
    url: '/admin/member/dormitory/detail',
    method: 'get',
    params: data
  })
}

export function edit(data) {
  return request({
    url: '/admin/member/dormitory/edit',
    method: 'post',
    data
  })
}

export function remove(data) {
  return request({
    url: '/admin/member/dormitory/remove',
    method: 'get',
    params: data
  })
}

