import request from '@/utils/request'

export function page(data) {
	return request({
		url: '/admin/member/info/page',
    method: 'post',
    data
  })
}
export function list(data) {
  return request({
    url: '/admin/member/info/list',
    method: 'get',
    params: data
  })
}

export function detail(data) {
  return request({
    url: '/admin/member/info/detail',
    method: 'get',
    params: data
  })
}

export function edit(data) {
  return request({
    url: '/admin/member/info/edit',
    method: 'post',
    data
  })
}

export function remove(data) {
  return request({
    url: '/admin/member/info/remove',
    method: 'get',
    params: data
  })
}

