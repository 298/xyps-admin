import request from '@/utils/request'

export function page(data) {
	return request({
		url: '/admin/xyps/agent/page',
    method: 'post',
    data
  })
}


export function agentList(data) {
  return request({
    url: '/admin/xyps/agent/agentList',
    method: 'get',
    params : data
  })
}

export function detail(data) {
  return request({
    url: '/admin/xyps/agent/detail',
    method: 'get',
    params: data
  })
}

export function edit(data) {
  return request({
    url: '/admin/xyps/agent/edit',
    method: 'post',
    data
  })
}

export function remove(data) {
  return request({
    url: '/admin/xyps/agent/remove',
    method: 'get',
    params: data
  })
}

