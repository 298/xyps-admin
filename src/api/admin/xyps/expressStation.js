import request from '@/utils/request'

export function page(data) {
	return request({
		url: '/admin/xyps/express-station/page',
    method: 'post',
    data
  })
}

//获取学校列表信息
export function getSchoolList(data) {
  return request({
    url: '/admin/system/area/schoolList',
    method: 'post',
    data
  })
}

export function detail(data) {
  return request({
    url: '/admin/xyps/express-station/detail',
    method: 'get',
    params: data
  })
}

export function edit(data) {
  return request({
    url: '/admin/xyps/express-station/edit',
    method: 'post',
    data
  })
}

export function remove(data) {
  return request({
    url: '/admin/xyps/express-station/remove',
    method: 'get',
    params: data
  })
}

