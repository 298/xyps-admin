import request from '@/utils/request'

export function page(data) {
	return request({
		url: '/admin/xyps/agent-area/page',
    method: 'post',
    data
  })
}

export function detail(data) {
  return request({
    url: '/admin/xyps/agent-area/detail',
    method: 'get',
    params: data
  })
}

export function edit(data) {
  return request({
    url: '/admin/xyps/agent-area/edit',
    method: 'post',
    data
  })
}

export function remove(data) {
  return request({
    url: '/admin/xyps/agent-area/remove',
    method: 'get',
    params: data
  })
}

