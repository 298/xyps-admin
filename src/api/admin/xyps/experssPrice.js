import request from '@/utils/request'

export function page(data) {
	return request({
		url: '/admin/xyps/experss-price/page',
    method: 'post',
    data
  })
}

export function detail(data) {
  return request({
    url: '/admin/xyps/experss-price/detail',
    method: 'get',
    params: data
  })
}

export function edit(data) {
  return request({
    url: '/admin/xyps/experss-price/edit',
    method: 'post',
    data
  })
}

export function remove(data) {
  return request({
    url: '/admin/xyps/experss-price/remove',
    method: 'get',
    params: data
  })
}

