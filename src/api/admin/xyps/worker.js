import request from '@/utils/request'

export function page(data) {
	return request({
		url: '/admin/xyps/worker/page',
    method: 'post',
    data
  })
}

//查询所有代理商信息
export function agentList(data) {
  return request({
    url: '/admin/xyps/agent/agentList',
    method: 'get',
    params : data
  })
}

export function detail(data) {
  return request({
    url: '/admin/xyps/worker/detail',
    method: 'get',
    params: data
  })
}

export function edit(data) {
  return request({
    url: '/admin/xyps/worker/edit',
    method: 'post',
    data
  })
}

export function remove(data) {
  return request({
    url: '/admin/xyps/worker/remove',
    method: 'get',
    params: data
  })
}

