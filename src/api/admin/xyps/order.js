import request from '@/utils/request'

// 查询所有配送员信息
export function workerList(data) {
  return request({
    url: '/admin/xyps/worker/list',
    method: 'post',
    data
  })
}

//查询所有代理商信息
export function agentList(data) {
  return request({
    url: '/admin/xyps/agent/agentList',
    method: 'get',
    params : data
  })
}

//查询所有快递站点信息
export function stationList(data) {
  return request({
    url: '/admin/xyps/express-station/list',
    method: 'get',
    params : data
  })
}

//查询所有学校信息
export function schoolList(data) {
  return request({
    url: '/admin/system/area/areaAllList',
    method: 'get',
    params : data
  })
}

export function page(data) {
	return request({
		url: '/admin/xyps/order/page',
    method: 'post',
    data
  })
}

export function detail(data) {
  return request({
    url: '/admin/xyps/order/detail',
    method: 'get',
    params: data
  })
}

export function edit(data) {
  return request({
    url: '/admin/xyps/order/edit',
    method: 'post',
    data
  })
}

export function editWorker(data) {
    return request({
        url: '/admin/xyps/order/editWorker',
        method: 'post',
        data
    })
}



export function remove(data) {
  return request({
    url: '/admin/xyps/order/remove',
    method: 'get',
    params: data
  })
}

