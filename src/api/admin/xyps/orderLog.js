import request from '@/utils/request'

export function page(data) {
	return request({
		url: '/admin/xyps/order-log/page',
    method: 'post',
    data
  })
}

export function detail(data) {
  return request({
    url: '/admin/xyps/order-log/detail',
    method: 'get',
    params: data
  })
}

export function edit(data) {
  return request({
    url: '/admin/xyps/order-log/edit',
    method: 'post',
    data
  })
}

export function remove(data) {
  return request({
    url: '/admin/xyps/order-log/remove',
    method: 'get',
    params: data
  })
}

