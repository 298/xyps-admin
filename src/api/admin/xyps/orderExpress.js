import request from '@/utils/request'

export function page(data) {
	return request({
		url: '/admin/xyps/order-express/page',
    method: 'post',
    data
  })
}

export function detail(data) {
  return request({
    url: '/admin/xyps/order-express/detail',
    method: 'get',
    params: data
  })
}

export function edit(data) {
  return request({
    url: '/admin/xyps/order-express/edit',
    method: 'post',
    data
  })
}

export function remove(data) {
  return request({
    url: '/admin/xyps/order-express/remove',
    method: 'get',
    params: data
  })
}

