import request from '@/utils/request'

//查看云存储配置
export function seecloud(data) {
	return request({
		url: '/admin/config/oss',
		method: 'GET',
		params:data
	})
}

//保存云存储配置
export function savecloud(data) {
	return request({
		url: '/admin/config/oss',
		method: 'POST',
		data
	})
}

//查看短信配置
export function seemess(data) {
	return request({
		url: '/admin/config/sms',
		method: 'GET',
		params:data
	})
}

//保存短信配置
export function savemess(data) {
	return request({
		url: '/admin/config/sms',
		method: 'POST',
		data
	})
}

//查看广告图配置
export function seeadv(data) {
	return request({
		url: '/admin/config/banner',
		method: 'GET',
		params:data
	})
}

//保存广告图配置
export function saveadv(data) {
	return request({
		url: '/admin/config/banner',
		method: 'POST',
		data
	})
}

//查看微信配置
export function seewec(data) {
	return request({
		url: '/admin/config/wechat',
		method: 'GET',
		params:data
	})
}

//保存微信配置
export function savewec(data) {
	return request({
		url: '/admin/config/wechat',
		method: 'POST',
		data
	})
}

//查看云打印机配置
export function seeprint(data) {
	return request({
		url: '/admin/config/print',
		method: 'GET',
		params:data
	})
}

//保存云打印机配置
export function saveprint(data) {
	return request({
		url: '/admin/config/print',
		method: 'POST',
		data
	})
}