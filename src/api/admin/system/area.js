import request from '@/utils/request'

export function page(data) {
	return request({
		url: '/admin/system/area/page',
    method: 'post',
    data
  })
}

export function areaList(data) {
  return request({
    url: '/admin/system/area/areaList',
    method: 'get',
    params : data
  })
}

export function schoolList(data) {
  return request({
    url: '/admin/system/area/schoolList',
    method: 'get',
    params: data
  })
}

export function areaAllList(data) {
  return request({
    url: '/admin/system/area/areaAllList',
    method: 'get',
    params : data
  })
}

export function agentAreaList(data) {
  return request({
    url: '/admin/system/area/agentAreaList',
    method: 'get',
    params : data
  })
}


export function detail(data) {
  return request({
    url: '/admin/system/area/detail',
    method: 'get',
    params: data
  })
}

export function edit(data) {
  return request({
    url: '/admin/system/area/edit',
    method: 'post',
    data
  })
}

export function remove(data) {
  return request({
    url: '/admin/system/area/remove',
    method: 'get',
    params: data
  })
}

