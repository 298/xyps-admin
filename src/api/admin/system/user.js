import request from '@/utils/request'

//用户列表
export function userPage(data) {
	return request({
		url: '/admin/system/user/page',
		method: 'post',
		data
	})
}

export function agentUserList(data) {
	return request({
		url: '/admin/system/user/agentUserList',
		method: 'get',
		params:data
	})
}

//角色列表
export function rolePage(data) {
	return request({
		url: '/admin/system/role/page',
		method: 'post',
		data
	})
}

//角色详情
export function roleDetail(data) {
	return request({
		url: '/admin/system/role/detail',
		method: 'post',
		data
	})
}

//修改密码
export function passwords(data) {
  return request({
    url: '/admin/profile/password',
    method: 'GET',
    params:data
  })
}

//检查登录名是否重复
export function checkUserName(data) {
	return request({
		url: '/admin/system/user/checkUserName',
		method: 'get',
		params: data
	})
}

//用户详情
export function userDetail(data) {
	return request({
		url: '/admin/system/user/detail',
		method: 'get',
		params: data
	})
}

//用户编辑
export function userEdit(data) {
	return request({
		url: 'admin/system/user/edit',
		method: 'post',
		data
	})
}

//删除用户
export function userDel(data) {
	return request({
		url: 'admin/system/user/del',
		method: 'get',
		params: data
	})
}

//重置密码
export function userReset(data) {
	return request({
		url: 'admin/system/user/reset',
		method: 'get',
		params: data
	})
}

//头像上传
export function ossUpload(data) {
	return request({
		url: 'admin/component/oss/base64Upload',
		method: 'post',
		params: data
	})
}
//修改头像
export function editAvatar(data) {
	return request({
		url: 'admin/system/user/editAvatar',
		method: 'post',
		params: data
	})
}
