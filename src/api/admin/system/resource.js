import request from '@/utils/request'

//资源树
export function resourceTree(data){
	return request({
		url: 'admin/system/resource/tree',
		method: 'get',
		params: data
	})
}

//校验权限标识
export function checkPermission(data){
	return request({
		url: 'admin/system/resource/checkPermission',
		method: 'get',
		params: data
	})
}

//上级菜单
export function parentTree(data){
	return request({
		url: 'admin/system/resource/parentTree',
		method: 'get',
		params: data
	})
}

//提交
export function edit(data){
	return request({
		url: 'admin/system/resource/edit',
		method: 'post',
		data
	})
}

//删除
export function resourceDel(data){
	return request({
		url: 'admin/system/resource/del',
		method: 'get',
		params: data
	})
}

//资源详情
export function manageData(data){
	return request({
		url: 'admin/system/resource/manageData',
		method: 'get',
		params: data
	})
}

export let types = [
  {
    value: 'MENU',
    label: '菜单'
  },
  {
    value: 'PERMISSION',
    label: '权限'
  }
];

export let typeMap = {};
types.forEach(item => typeMap[item.value] = item.label)
