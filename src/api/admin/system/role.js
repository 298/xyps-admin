import request from '@/utils/request'

//角色列表
export function rolePage(data) {
	return request({
		url: '/admin/system/role/page',
		method: 'post',
		data
	})
}

//角色详情
export function roleDetail(data) {
	return request({
		url: '/admin/system/role/detail',
		method: 'post',
		data
	})
}

// 检查角色标识是否重复
export function checkRoleIdent(data) {
	return request({
		url: '/admin/system/role/checkRole',
		method: 'get',
		params: data
	})
}

// 角色编辑
export function rolEdit(data) {
	return request({
		url: '/admin/system/role/edit',
		method: 'post',
		data
	})
}

//删除角色
export function roleDel(data) {
	return request({
		url: '/admin/system/role/del',
		method: 'get',
		params: data
	})
}

//资源树
export function resourceTree(data){
	return request({
		url: 'admin/system/resource/tree',
		method: 'get',
		params: data
	})
}