import request from '@/utils/request'
import {getToken, removeToken} from '@/utils/auth'

function getSearch(data) {
  return request({
    url: '/admin/music/kuwo/search',
    method: 'post',
    data
  })
}

function getDetail(data) {
  return request({
    url: '/admin/music/kuwo/detail',
    method: 'post',
    data
  })
}

function getUrl(data) {
  return request({
    url: '/admin/music/kuwo/url',
    method: 'post',
    data
  })
}

export default {
  getSearch,
  getDetail,
  getUrl,
};
