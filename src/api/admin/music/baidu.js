import request from '@/utils/request'
import {getToken, removeToken} from '@/utils/auth'

function search(data) {
  return request({
    url: '/admin/music/baidu/search',
    method: 'post',
    data
  })
}

function detail(data) {
  return request({
    url: '/admin/music/baidu/detail',
    method: 'post',
    data
  })
}

export default {
  search,
  detail,
  proxy: process.env.VUE_APP_BASE_API + '/admin/music/baidu/proxy?token=' + getToken()
};
