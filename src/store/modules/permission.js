import {
  asyncRoutes,
  constantRoutes
} from '@/router'
/* Router Modules */
/* Layout */
import Layout from '@/layout'
import {isNotNull} from '@/utils'

import api from '@/api/admin/menu' //调用模块接口

/**
 * Use meta.role to determine if the current user has permission
 * @param roles
 * @param route
 */
function hasPermission(roles, route) {
  if (route.meta && route.meta.roles) {
    return roles.some(role => route.meta.roles.includes(role))
  } else {
    return true
  }
}

/**
 * Filter asynchronous routing tables by recursion
 * @param routes asyncRoutes
 * @param roles
 */
export function filterAsyncRoutes(routes, roles) {
  const res = []

  routes.forEach(route => {
    const tmp = {
      ...route
    }
    if (hasPermission(roles, tmp)) {
      if (tmp.children) {
        tmp.children = filterAsyncRoutes(tmp.children, roles)
      }
      res.push(tmp)
    }
  })

  return res
}

/**
 * 根据服务器传来的树结构拼接成router对应的菜单
 * @param {Object} item
 */
const permissionButtons = new Set();

export function myAsyncRoutes(item, flag) {
  const res = []
  item.forEach(items => {
    if (items.path) {
      if (isNotNull(items.permission)) {
        permissionButtons.add(items.permission);
      }
      let d = {}
      //通用的配置
      d.path = (!isNotNull(flag) ? '/' : '') + items.path
      d.meta = {
        title: items.name || '未配置',
        icon: items.icon || 'el-icon-position'
      }
      d.name = items.name || '未配置';
      //一级路由配置
      //一级路由配置
      if (!flag) {
        d.component = Layout
        d.redirect = items.redirect ? 'noRedirect' : items.redirect
        d.alwaysShow = true
      }
      d.hidden = items.isHidden
      //如果有子级并且url等于空，则递归获取子级
      if (!isNotNull(items.url) && isNotNull(items.children)) {
        d.children = myAsyncRoutes(items.children, true)
      } else {
        if (isNotNull(items.url)) {
          if (flag) {//如果是递归进来的子级，直接返回子级组件
            d.component = (resolve) => require([`@/pages/${items.url}`], resolve)
          } else {
            let isHttp = items.url.indexOf('http') !== -1
            //当只有一级的时候路由配置
            d.path = isHttp ? items.path : '/'
            d.alwaysShow = false
            d.component = Layout
            delete d.meta
            d.children = [{
              path: isHttp ? items.url : items.path,
              component: isHttp ? null : (resolve) => require([`@/pages/${items.url}`], resolve),
              meta: {
                title: items.name || '未配置title',
                icon: items.icon || 'el-icon-position',
                noCache: true,
                affix: true
              }
            }]
          }
        }
      }
      res.push(d)
    }
  })
  return res
}

const state = {
  routes: [],
  addRoutes: [],
  buttons: [] // 安装权限
}

const mutations = {
  SET_ROUTES: (state, routes) => {
    state.addRoutes = routes
    state.routes = constantRoutes.concat(routes)
  },
  SET_BUTTONS: (state, buttons) => {
    state.buttons = buttons
  }
}

const actions = {
  generateRoutes({
                   commit
                 }, roles) {
    return new Promise(resolve => {
      let accessedRoutes = [];
      if (roles.includes('admin')) {
        accessedRoutes = asyncRoutes || []
      } else {
        accessedRoutes = filterAsyncRoutes(asyncRoutes, roles)
      }
      // commit('SET_ROUTES', accessedRoutes)
      // resolve(accessedRoutes)
      //菜单路由通过接口过来
      //模拟ajax获取菜单
      api.resourceMenuTree().then(res => {
        if (res.success) {
          accessedRoutes = myAsyncRoutes(res.data.menus).concat(accessedRoutes)
        }
        commit('SET_BUTTONS', permissionButtons)
        accessedRoutes.push({
          id: '1',
          path: '/person',
          component: Layout,
          redirect: 'noRedirect',
          hidden: true,
          name: 'person',
          meta: {
            title: '个人中心',
            icon: 'user'
          },
          children: [
            {
              id: '2',
              path: 'setting',
              component: () => import('@/pages/admin/person'),
              name: 'setting',
              meta: {
                title: '设置'
              }
            }
          ]
        }, {
          id: '3',
          path: '*',
          redirect: '/404',
          hidden: true
        })
        commit('SET_ROUTES', accessedRoutes)
        resolve(accessedRoutes)
      })
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
